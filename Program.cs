﻿using System.Threading;

namespace hw_5
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            CancellationTokenSource source = new();
            CancellationToken token = source.Token;

            var path = @"D:\repos\tests";
            var files = new List<string>();

            var enumers = new EnumFiles();
            enumers.FileFound += (sender, e) => {
                Console.WriteLine($"Файл - {e.Name}");
                files.Add(e.Name);

                if (files.Count > 100)
                {
                    source.Cancel();
                }
            };
            enumers.CrawlFiles(path,token);

            var maxElement = files.GetMax<string>((name) => {return name.Length;});

            Console.WriteLine($"Максимум: {maxElement}");
        }
    }
}