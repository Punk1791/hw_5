using System.Collections;
using System.Linq;

namespace hw_5
{
    public static class Extence
    {
        public static T GetMax<T>(this IEnumerable e, Func<T, float> getParameter) where T : class
        {
            var list = new Dictionary<T, float>();

            foreach (T i in e)
            {
                list.Add(i,getParameter(i));
            }

            return list.Aggregate((l, r) => l.Value > r.Value ? l : r).Key;
        }
    }
}
