﻿using System.Threading;
using System.IO;

namespace hw_5
{
    public class FileName : EventArgs
    {
        public string Name {get; set;}
    }

    public class EnumFiles
    {
        public delegate void EventHandler(object sender, FileName NameFile);
        public event EventHandler FileFound;
        public void CrawlFiles(string path, CancellationToken cancelToken)
        {
            foreach (FileInfo i in new DirectoryInfo(path).EnumerateFiles())
            {
                FileName name = new();
                name.Name = i.FullName;
                FileFound?.Invoke(this, name);

                if (cancelToken.IsCancellationRequested)
                {
                    return;
                }
            }
        }
    }
}

